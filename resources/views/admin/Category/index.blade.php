@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}
                    <button class="btn btn-success float-right">Create New Category</button>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <table class="table table-hover">
                            <thead>
                                <th>
                                Category Name 
                                </th>
                                
                                <th>
                                Editing
                                </th>
                                
                                <th>
                                Deleting
                                </th>

                            </thead>
                            <tbody>
                            @foreach($category as $category)
                                <tr>
                                    <td>
                                        {{ $category->name }}
                                    </td>
                                    <td>
                                    <a href="/admin/category/edit/{{$category->id}}" class="btn btn-info">
                                    Edit
                                    </a>
                                    </td>
                                    <td>
                                    <a href="/admin/category/delete/{{$category->id}}" class="btn btn-danger">x</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
